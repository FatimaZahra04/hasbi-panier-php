<?php

error_reporting(0);

include("header.php");

session_start();

if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $pass=md5($_POST['password']);
    $sql3 = "select * from user where email ='$email' and password ='$pass' ";
    $result4=mysqli_query($con,$sql3);
    if ($result4->num_rows>0) {
        $row=mysqli_fetch_assoc($result4);
        $_SESSION['first_name']=$row['first_name'];
        header("Location:index.php");
    }
    else {
        echo "<script>  alert('Email or password incorrect !');  </script>";
    }
}

?>
<section class="bg-dark">
    <div class="container p-4">
        <!--Login form -->
        <div class="row">
            <div class="col-lg-4 offset-lg-4 bg-light rounded" id="login-box">
                <h2 class="text-center mt-2">Login</h2>
                <form method="post" role="form" class="p-2" id="login-form">
                    <div class="form-group">
                        <input type="email" name="email" value="<?= $email ?>" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" value="<?= $_POST['password'] ?>" class="form-control" placeholder="Password" required>
                    </div>
                        <div class="form-check mb-2">
                            <input type="checkbox" name="rememberMe" class="form-check-input" id="customCheck">
                            <label for="customCheck" class="form-check-label">Remember Me</label>
                            <a href="forgotPass.php" id="forgot-btn" class="float-right">Forgot Password ?</a>
                        </div>
                    <div class="form-group">
                        <input type="submit" name="login" id="login" value="login" class="btn btn-primary btn-block">
                    </div>
                    <div class="form-group">
                        <p class="text-center">Don't have an account ?
                            <a href="register.php" id="register-btn">Register Here</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
        <!--Login form end-->

    </div>
</section>

    <hr class="m-0">
<?php
include("footer.php");
?>