<?php
    $product_id=$_GET['product_id'] ?? 1;
    foreach ($product->getData() as $item):
        if ($item['sku']==$product_id) :
            if ($_SERVER['REQUEST_METHOD']=="POST") {
                if (isset($_POST["product_submit"])) {

                    //calling addToCart function
                    $cart->addToCart($_POST['user_id'],$_POST['sku_p']);
                }
            }
?>

<!-- product -->
<section id="product" class="py-3">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 py-5 text-center">
                <img src="<?= $item['image'] ?? "" ?>" alt="" class="img-fluid align-items-center" style="height: 400px" width="400">
                <div class="form-row pt-4 font-size-16">
                    <div class="col">
                        <form method="post">
                            <input type="hidden" name="sku_p" value="<?= $item['sku'] ?? 0?>">
                            <input type="hidden" name="user_id" value="<?= 1 ?>">
                            <?php
                            if (in_array($item['sku'],$cart->getCartId($product->getData("select * from cart")) ?? [])) {
                                echo ' <button type="submit" disabled class="btn btn-success font-size-14 form-control">In the Cart</button>';
                            }
                            else {
                                echo ' <button type="submit" name="product_submit" class="btn btn-warning  font-size-14 form-control">Add to Cart</button>';
                            }
                            ?>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 py-5">
                <h5 class="font-size-20"><?= $item['name'] ?? "Unknown" ?></h5>
                <div class="d-flex">
                    <div class="rating text-warning font-size-12">
                        <span> <i class="fas fa-star"></i></span>
                        <span> <i class="fas fa-star"></i></span>
                        <span> <i class="fas fa-star"></i></span>
                        <span> <i class="fas fa-star"></i></span>
                        <span> <i class="fas fa-star"></i></span>
                        <span> <i class="fas fa-star"></i></span>
                    </div>
                </div>
                <hr class="m-0">

                <!--product price -->
                <table class="my-3">
                    <tr class="font-size-14">
                        <td>Price</td>
                        <td class="font-size-20 text-danger">&nbsp;&nbsp;&nbsp;<span><?= $item['price'] ?? 0 ?></span>$
                            &nbsp;&nbsp;&nbsp;<small class="text-dark font-size-12">inclusive of all taxes</small></td>
                    </tr>

                    <tr class="font-size-14">
                        <td>Type</td>
                        <td>&nbsp;&nbsp;&nbsp;<span class="font-size-16 text-info"><?= $item['type'] ?? ""?></span> </td>
                    </tr>
                    <tr class="font-size-14">
                        <td>Shipping price</td>
                        <td>&nbsp;&nbsp;&nbsp;<span class="font-size-16 text-info"><?= $item['shipping'] ?? 0?>$</span> </td>
                    </tr>
                    <tr class="font-size-14">
                        <td>Model</td>
                        <td>&nbsp;&nbsp;&nbsp;<span class="font-size-16 text-info"><?= $item['model'] ?? ""?></span> </td>
                    </tr>
                    <tr class="font-size-14">
                        <td>Manufacturer</td>
                        <td>&nbsp;&nbsp;&nbsp;<span class="font-size-16 text-info"><?= $item['manufacturer'] ?? ""?></span> </td>
                    </tr>

                </table>
                <!--product price -->

                <!--policy -->

                <div id="policy">
                    <div class="d-flex">
                        <div class="return text-center mr-5">
                            <div class="font-size-20 my-2 text-info">
                                        <span class="fas fa-retweet border p-3 rounded-pill">
                                        </span>
                            </div>
                            <a href="#" class="font-size-12">10 Days <br>Replacement</a>
                        </div>
                        <div class="return text-center mr-5">
                            <div class="font-size-20 my-2 text-info">
                                        <span class="fas fa-truck border p-3 rounded-pill">
                                        </span>
                            </div>
                            <a href="#" class="font-size-12">Amana<br>Delivery</a>
                        </div>
                        <div class="return text-center mr-5">
                            <div class="font-size-20 my-2 text-info">
                                        <span class="fas fa-check-double border p-3 rounded-pill">
                                        </span>
                            </div>
                            <a href="#" class="font-size-12">1 Year<br>Warranty</a>
                        </div>
                    </div>
                </div>

                <!--policy -->
                <hr>
                <!-- order details -->
                <div id="order-details" class="d-flex flex-column text-dark">
                    <small>Delivery time : June 15 - June 30</small>
                    <small>Sold by <a href="#">Electronics Coda Shop</a> </small>
                </div>
                <!--order details -->
            </div>
            <div class="col-12 mt-1">
                <h5>Product description</h5>
                <hr>
                <p class="font-size-14">
                    <?= $item['description'] ?? ""?>
                </p>
            </div>
        </div>
    </div>
</section>

<?php
   endif;
    endforeach;
        ?>