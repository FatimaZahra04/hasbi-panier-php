<?php
if ($_SERVER['REQUEST_METHOD']=='POST') {
    if (isset($_POST['delete-cart-submit'])) {
        $deletedRecord = $cart->deleteCart($_POST['sku_p']);
    }
    if (isset($_POST['wishlist-submit'])) {
                $cart->saveForLater($_POST['sku_p']);
    }
}
?>
<section id="cart" class="py-3 mb-2">
    <div class="container">
        <h5 class="font-size-20">Shopping Cart</h5>
        <!--  shopping cart items   -->
        <div class="row">
            <div class="col-sm-9">
                <?php
                foreach ($product->getData("select * from cart") as $item):
                    $ProductSku=$product->getProduct($item['sku']);
                      $sub_total[]=array_map(function($item){
                ?>
                <!-- cart item -->
                <div class="row border-top py-3">
                    <div class="col-sm-2">
                        <img src="<?= $item['image'] ?>" class="img-fluid">
                    </div>
                    <div class="col-sm-8">
                        <h5 class="font-size-16"><?= $item['name'] ?></h5>
                        <!-- product rating -->
                        <div class="d-flex">
                            <div class="rating text-warning font-size-12">
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="far fa-star"></i></span>
                            </div>
                        </div>
                        <!--  !product rating-->

                        <!-- product qty -->
                        <div class="qty d-flex pt-2">
                            <div class="d-flex w-25">
                                <button class="qty-up border bg-light" data-id="<?php echo $item['sku'] ?? '0'; ?>"><i class="fas fa-angle-up"></i></button>
                                <input type="number" data-id="<?php echo $item['sku'] ?? '0'; ?>" class="qty_input border px-2 w-100 bg-light" disabled value="1">
                                <button data-id="<?php echo $item['sku'] ?? '0'; ?>" class="qty-down border bg-light"><i class="fas fa-angle-down"></i></button>
                            </div>
                            <form method="post">
                                <input type="hidden" value="<?= $item['sku'] ?? 0; ?>" name="sku_p">
                                <button type="submit" name="delete-cart-submit" class="btn text-danger px-3 border-right">Delete</button>
                            </form>
                            <form method="post">
                                <input type="hidden" value="<?= $item['sku'] ?? 0; ?>" name="sku_p">
                                <button type="submit" name="wishlist-submit" class="btn text-danger">Save for Later</button>
                            </form>

                        </div>
                        <!-- !product qty -->

                    </div>

                    <div class="col-sm-2 text-right">
                        <div class="font-size-14 text-danger">
                          <span>price</span>&nbsp;&nbsp;
                            $<span class="product_price" data-id="<?php echo $item['sku'] ?? '0'; ?>"><?= $item['price'] ?? 0; ?></span>
                        </div>
                        <div class="font-size-14 text-info">
                            <span>
                                <?php if ($item['shipping']==0) echo '';
                                else echo "shipping";
                            echo "</span>&nbsp;&nbsp;
                            <span class='product_shipping'>";
                                if ($item['shipping']==0) echo 'free shipping';
                            else echo "$".$item['shipping']?>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- !cart item -->

                <?php
                          return $item['price']+$item['shipping'];
                        },$ProductSku); //closing array map
                endforeach;
                ?>
            </div>
            <!-- subtotal section-->
            <div class="col-sm-3">
                <div class="sub-total border text-center mt-2">
                    <div class="border-top py-4">
                        <h5 class="font-size-20">Subtotal (<?php echo isset($sub_total) ? count($sub_total) : 0; ?> item):&nbsp;<span class="text-danger">$<span class="text-danger" id="Totalprice">
                                    <?php echo isset($sub_total) ? $cart->getSum($sub_total) : 0; ?></span> </span> </h5>

                            <a href="<?php  echo isset($_SESSION['first_name'])? "index.php" : "Login.php" ?>"
                        <button type="submit"  class="btn btn-warning mt-3" id="buy"> Proceed to Buy
                           </button>
                        </a>
                    </div>
                    </div>
                </div>
            </div>
            <!-- !subtotal section-->
        </div>
        <!--  !shopping cart items   -->
    </div>
</section
<!-- !Shopping cart section  -->
