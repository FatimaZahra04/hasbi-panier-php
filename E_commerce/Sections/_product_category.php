<?php
if ($_SERVER['REQUEST_METHOD']=='POST') {
    if (isset($_POST['product_category_submit'])) {

        //calling addToCart function
        $cart->addToCart($_POST['user_id'], $_POST['sku_p']);
    }
}

?>
<!-- filter product -->
<section id="filter-product">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <h4 class="font-size-20 text-center">Filter Product</h4>
                <hr>
                <h6 class="text-info text-center">Select Category</h6>
                <ul class="list-group">
                    <?php foreach ($category_shuffle as $item) {
                        ?>
                        <li class="list-group-item">
                            <div class="form-check">
                                <div class="form-check-label">
                                    <input type="checkbox" class="form-check-input product_check" value="<?php echo $item['name']; ?>" id="category">
                                    <?php
                                   echo $item['name']." (".$item['nproducts'].")";
                                    ?>
                                </div>
                            </div>
                        </li>
                    <?php
                        } ?>
                </ul>
            </div>
            <div class="col-lg-9">
                <h5 class="text-center" id="textChange">All Products</h5>
                <hr>
                <div class="text-center mr-2" style="margin-top: 43px">
                    <img src="assets/loader.gif" id="loader" width="200"
                    style="display: none;">
                    <div class="row" id="result">
                <?php  include "pagination.php"?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- filter product -->
</main>
<!-- end #main-site-->

