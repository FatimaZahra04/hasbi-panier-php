<?php
if ($_SERVER['REQUEST_METHOD']=='POST') {
    if (isset($_POST['delete-wishlist-submit'])) {
        $deletedRecord = $cart->deleteCart($_POST['sku_p'],'wishlist');
    }
    if (isset($_POST['cart-submit'])) {

        $cart->saveForLater($_POST['sku_p'],'cart','wishlist');
    }
}
?>
<section id="cart" class="py-3 mb-5">
    <div class="container">
        <h5 class="font-size-20">Wishlist</h5>
        <!--  wishlist items   -->
        <div class="row">
            <div class="col-sm-9">
                <?php
                foreach ($product->getData("select * from wishlist") as $item):
                    $ProductSku=$product->getProduct($item['sku']);
                    $sub_total[]=array_map(function($item){
                        ?>
                        <!-- cart item -->
                        <div class="row border-top py-3">
                            <div class="col-sm-2">
                                <img src="<?= $item['image'] ?>" class="img-fluid">
                            </div>
                            <div class="col-sm-8">
                                <h5 class="font-size-16"><?= $item['name'] ?></h5>
                                <!-- product rating -->
                                <div class="d-flex">
                                    <div class="rating text-warning font-size-12">
                                        <span><i class="fas fa-star"></i></span>
                                        <span><i class="fas fa-star"></i></span>
                                        <span><i class="fas fa-star"></i></span>
                                        <span><i class="fas fa-star"></i></span>
                                        <span><i class="far fa-star"></i></span>
                                    </div>
                                </div>
                                <!--  !product rating-->

                                <div class="d-flex pt-2">
                                    <form method="post">
                                        <input type="hidden" value="<?= $item['sku'] ?? 0; ?>" name="sku_p">
                                        <button type="submit" name="delete-wishlist-submit" class="btn text-danger pl-0 pr-3 border-right">Delete</button>
                                    </form>
                                    <form method="post">
                                        <input type="hidden" value="<?= $item['sku'] ?? 0; ?>" name="sku_p">
                                        <button type="submit" name="cart-submit" class="btn text-danger">Add to Cart</button>
                                    </form>

                                </div>

                            </div>

                            <div class="col-sm-2 text-right">
                                <div class="font-size-14 text-danger">
                                    <span>price</span>&nbsp;&nbsp;
                                    $<span class="product_price" data-id="<?php echo $item['sku'] ?? '0'; ?>"><?= $item['price'] ?? 0; ?></span>
                                </div>
                                <div class="font-size-14 text-info">
                            <span>
                                <?php if ($item['shipping']==0) echo '';
                                else echo "shipping";
                                echo "</span>&nbsp;&nbsp;
                            <span class='product_shipping'>";
                                if ($item['shipping']==0) echo 'free shipping';
                                else echo "$".$item['shipping']?>
                            </span>
                                </div>
                            </div>
                        </div>
                        <!-- !cart item -->

                        <?php
                        return $item['price']+$item['shipping'];
                    },$ProductSku); //closing array map
                endforeach;
                ?>
            </div>
        </div>
        <!--  wishlist items   -->
    </div>
</section>
<!-- wishlist section  -->
