<?php

require ('../database/DBController.php');

require ('../database/Product.php');

$db = new DBController();

$product = new Product($db);

if (isset($_POST['sku'])){
    $result = $product->getProduct($_POST['sku']);
    echo json_encode($result);
}
