<?php
if ($_SERVER['REQUEST_METHOD']=="POST") {
    if (isset($_POST["special_price_submit"])) {

        //calling addToCart function
        $cart->addToCart($_POST['user_id'],$_POST['sku_p']);
    }
}
?>
<section id="top-sale">
    <div class="container py-5">
        <h4 class="font-size-20">Special Price</h4>
        <hr>
        <!-- owl carousel -->
        <div class="owl-carousel owl-theme">
            <?php foreach ($product_shuffle as $item) {
                if ($item['price'] < 6) {
                ?>
            <div class="item py-2 mr-3">
                <div class="product">
                    <a href="<?php  printf('%s?product_id=%s','./product.php',$item['sku']) ?>">
                        <img src="<?php
                        echo   $item['image'];?>" class="img-fluid" style="height: 200px">
                    </a>
                    <div class="text-center">
                        <h6>
                            <?php

                            $string=  str_replace(array("\t","\r", "\n"), '', $item['name']);
                            echo $string = (strlen($string) > 41) ? substr($string,0,41).'...' : $string;
                            ?>
                        </h6>
                        <div class="rating text-warning font-size-12">
                            <span> <i class="fas fa-star"></i></span>
                            <span> <i class="fas fa-star"></i></span>
                            <span> <i class="fas fa-star"></i></span>
                            <span> <i class="fas fa-star"></i></span>
                            <span> <i class="fas fa-star"></i></span>
                        </div>
                        <div class="price py-2">
                            <span>$<?= $item['price'] ?? '0'; ?></span>
                        </div>
                        <form action="" method="post">
                            <input type="hidden" name="sku_p" value="<?= $item['sku'] ?>">
                            <input type="hidden" name="user_id" value="<?= 1 ?>">
                            <?php
                            if (in_array($item['sku'],$cart->getCartId($product->getData("select * from cart")) ?? [])) {
                                echo ' <button type="submit" disabled class="btn btn-success font-size-12">In the Cart</button>';
                            }
                            else {
                                echo ' <button type="submit" name="special_price_submit" class="btn btn-warning font-size-12">Add to Cart</button>';
                            }
                            ?>

                        </form>
                    </div>
                </div>
            </div>
            <?php
                }
} ?>
        </div>
    </div>

</section>
