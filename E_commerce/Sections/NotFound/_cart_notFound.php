<section id="cart" class="py-3 mb-2">
    <div class="container">
        <h5 class="font-size-20">Shopping Cart</h5>
        <!--  shopping cart items   -->
        <div class="row">
            <div class="col-sm-9">
                <!-- empty cart -->
                <div class="row border-top py-3 mt-3">
                    <div class="col-sm-12 text-center py-2">
                        <img src="././assets/emptycart.png" class="img-fluid" style="height: 200px">
                    </div>
                </div>
                <!-- empty cart -->
            </div>
            <!-- subtotal section-->
            <div class="col-sm-3">
                <div class="sub-total border text-center mt-2">
                    <div class="border-top py-4">
                        <h5 class="font-size-20">Subtotal (<?php echo isset($sub_total) ? count($sub_total) : 0; ?> item):&nbsp;<span class="text-danger">$<span class="text-danger" id="Totalprice">
                                    <?php echo isset($sub_total) ? $cart->getSum($sub_total) : 0; ?></span> </span> </h5>
                        <button type="submit" class="btn btn-warning mt-3">Proceed to Buy</button>
                    </div>
                </div>
            </div>
            <!-- !subtotal section-->
        </div>
        <!--  !shopping cart items   -->
    </div>
</section>
<!-- !Shopping cart section  -->
