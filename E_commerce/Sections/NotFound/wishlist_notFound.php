<section id="cart" class="py-3 mb-5">
    <div class="container">
        <h5 class="font-size-20">Wishlist</h5>
        <!--  wishlist items   -->
        <div class="row">
            <div class="col-sm-9">
                <!-- empty wishlist -->
                <div class="row border-top py-3 mt-3">
                    <div class="col-sm-12 text-center py-2">
                        <img src="././assets/emptywishlist.png" class="img-fluid" style="height: 200px">
                    </div>
                </div>
                <!-- empty wishlist -->
            </div>
        </div>
        <!--  wishlist items   -->
    </div>
</section>
<!-- wishlist section  -->
