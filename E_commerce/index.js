$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        loop : true,
        nav : true,
        dots : true,
        responsive: {
            0 : {
                items : 0
            },
            576 : {
                items : 3
            },
            768 : {
                items : 4
            },
            992 : {
                items : 5
            }
        },
    });
    let $qty_up = $(".qty-up");
    let $qty_down = $(".qty-down");
    let $totalPrice=$('#Totalprice');


    $qty_up.click(function (e) {


        let $input = $(`.qty_input[data-id='${$(this).data("id")}']`);
        let $price=$(`.product_price[data-id='${$(this).data("id")}']`);

        //change product price according to qty using ajax call
        $.ajax({
            url: "Sections/ajax.php",
            type : 'POST',
            dataType : 'json',
            data : {sku : $(this).attr("data-id")},
            success:function (result) {
                let product_price=result[0]['price'];

                if (parseInt($input.val())  >= 1 && parseInt($input.val()) <=9) {
                    $input.val(function (i,oldval){
                        return ++oldval;
                    })

                    //increase price of the product

                    $price.text(parseFloat(product_price * $input.val()).toFixed(2));

                    //subtotal price
                    let subtotal=parseFloat($totalPrice.text()) + parseFloat(product_price);
                    $totalPrice.text(subtotal.toFixed(2));
                }

        }});//closing ajax request


    });

    $qty_down.click(function (e) {
        let $input = $(`.qty_input[data-id='${$(this).data("id")}']`);
        let $price=$(`.product_price[data-id='${$(this).data("id")}']`);

        $.ajax({
            url: "Sections/ajax.php",
            type : 'POST',
            dataType : 'json',
            data : {sku : $(this).attr("data-id")},
            success:function (result) {
                let product_price=result[0]['price'];

                if (parseInt($input.val()) > 1 && parseInt($input.val()) <= 10) {
                    $input.val(function (i,oldval){
                        return --oldval;
                    })
                    //deacrease price of the product

                    $price.text(parseFloat(product_price * $input.val()).toFixed(2));

                    //subtotal price
                    let subtotal=parseFloat($totalPrice.text()) - parseFloat(product_price);
                    $totalPrice.text(subtotal.toFixed(2));

                }

            }});//closing ajax request
    });

    //filter products by category

    $(".product_check").click(function () {
        let checkbox = $('input[type="checkbox"]');
        //uncheck previous checkbox once clicking the next one
        checkbox.not(this).prop('checked',false);
        //
        $("#loader").show();
        let category = get_filter_text('category');
        let action = 'data';
        $.ajax({
            url : 'action.php',
            method : 'POST',
            data : {action:action,category:category},
            success:function (response) {
                $('#result').html(response);
                $('#loader').hide();
                $('#textChange').text("Filtered Products");
            }
        });
    })
    function get_filter_text(text_id) {
        let filterData = [];

        $('#'+text_id+':checked').each(function () {
           filterData.push($(this).val());
        });
        return filterData;
    }

});


