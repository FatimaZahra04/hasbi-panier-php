<?php
include("header.php");
?>
<section class="bg-dark">
    <div class="container p-4">
<!--forgot password -->
<div class="row">
    <div class="col-lg-4 offset-lg-4 bg-light rounded" id="forgot-box">
        <h2 class="text-center mt-2">Reset password</h2>
        <form method="post" role="form" class="p-2" id="forgot-form">
            <div class="form-group">
                <small class="text-muted">
                    To reset your password, enter your email address !
                </small>
            </div>

            <div class="form-group">
                <input type="email" name="femail" class="form-control" placeholder="Email" required>
            </div>
            <div class="form-group">
                <input type="submit" name="forgotP" id="forgotP" value="Reset" class="btn btn-primary btn-block">
            </div>
            <div class="form-group">
                <a href="Login.php" id="back-btn">Back</a>
                </p>
            </div>
        </form>
    </div>
</div>
</div>
</section>

<hr class="m-0">

<?php
include("footer.php");
?>