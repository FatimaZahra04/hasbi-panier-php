<?php
//require mysql connection
require ("database/DBController.php");

//require product class
require ("database/Product.php");

//require cart class
require ("database/cart.php");


$db = new DBController();

$product = new Product($db);

$product_shuffle = $product->getData();

$category=new Product($db);

$category_shuffle=$category->getData('SELECT c.name, COUNT(DISTINCT p.sku) AS nproducts
FROM categories AS c
LEFT JOIN product_categories AS cp ON cp.id=c.id
LEFT JOIN product AS p ON p.sku=cp.sku
GROUP BY c.id,c.name
ORDER BY nproducts desc ;');

//cart object

$cart = new cart($db);
