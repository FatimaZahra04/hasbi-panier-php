<?php

    $file = file_get_contents('./Products/products.json');

    $fileData = json_decode($file,true);



$pdo = new PDO("mysql:host=localhost;dbname=boutique", 'root', '');

$result=$pdo->prepare("select count(*) from product");
$result->execute();
$row_count=$result->fetchColumn();

    $stmCategories = $pdo->prepare('INSERT INTO `categories` VALUES(:id, :name)');

    $stmProduct_Categories = $pdo->prepare('INSERT INTO `product_categories` VALUES(:sku, :id)');

    $stmProduct = $pdo->prepare('INSERT INTO `product` VALUES( :sku, :name, :type, :price, :upc, :shipping, :description, :manufacturer, :model, :url, :image)');

    $insetedCategories = array();
if ($row_count==0) {

    foreach ($fileData as $product) {
        $data = ["sku" => $product['sku'],
            "name" => $product['name'],
            "type" => $product['type'],
            "price" => $product['price'],
            "upc" => $product['upc'],
            "shipping" => $product['shipping'],
            "description" => $product['description'],
            "manufacturer" => $product['manufacturer'],
            "model" => $product['model'],
            "url" => $product['url'],
            "image" => $product['image']
        ];

        $stmProduct->execute($data);
        foreach ($product['category'] as $category) {

            if (!in_array($category['id'], $insetedCategories)) {
                $data_category = ["id" => $category['id'],
                    "name" => $category['name']
                ];
                array_push($insetedCategories, $data_category);

                $stmCategories->execute($data_category);

            }
            $data_category_product = ["sku" => $product['sku'],
                "id" => $category['id']];

            $stmProduct_Categories->execute($data_category_product);

        }
    }
}
