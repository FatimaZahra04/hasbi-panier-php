<?php
require "./functions.php";
require ("database/db.php");

    if ($_SERVER['REQUEST_METHOD']=='POST') {
    if (isset($_POST['product_category_submit'])) {

        //calling addToCart function
        $cart->addToCart($_POST['user_id'], $_POST['sku_p']);
    }
}

        if (isset($_POST['action']))
        {
            $limit = 48;
            if (isset($_GET['page_no'])) {
                $page_no = $_GET['page_no'];
            }else{
                $page_no = 1;
            }
            $offset = ($page_no-1) * $limit;


            $sqll = "SELECT DISTINCT * FROM categories AS c, product_categories AS cp,product AS p
                WHERE cp.id=c.id AND p.sku=cp.sku AND c.name !=''";

            if (isset($_POST['category']))
            {
                $categoryy = implode( $_POST['category']);
                $sqll .= "AND c.name IN('".$categoryy."')";

            }

            $sqll.="GROUP BY p.sku ORDER BY price ASC
            LIMIT $offset,$limit";

            $result= $con->query($sqll);

            if ($result->num_rows>0) {
                while ($row=$result->fetch_assoc()) {
                    ?>
            <div class="col-sm-3 py-2 border" style="width: 200px;">
                <div class="product">
        <a href="<?php  printf('%s?product_id=%s','product.php',$row['sku']) ?>"><img src="<?= $row['image'] ;?>" class="img-fluid" style="height: 200px"></a>
        <div class="text-center">
            <h6>
                <?php
                $string=  str_replace(array("\t","\r", "\n"), '', $row['name']);
                echo $string = (strlen($string) > 41) ? substr($string,0,41).'...' : $string;
                ?>
            </h6>
            <div class="rating text-warning font-size-12">
                <span> <i class="fas fa-star"></i></span>
                <span> <i class="fas fa-star"></i></span>
                <span> <i class="fas fa-star"></i></span>
                <span> <i class="fas fa-star"></i></span>
                <span> <i class="fas fa-star"></i></span>
            </div>
            <div class="price py-2">
                <span>$<?= $row['price']; ?></span>
            </div>
            <form method="post">
                <input type="hidden" name="sku_p" value="<?= $row['sku'] ?>">
                <input type="hidden" name="user_id" value="<?= 1 ?>">
                <?php
                if (in_array($row['sku'],$cart->getCartId($product->getData("select * from cart")) ?? [])) {
                    echo ' <button type="submit" disabled class="btn btn-success font-size-12">In the Cart</button>';
                }
                else {
                    echo ' <button type="submit" name="product_category_submit" class="btn btn-warning font-size-12">Add to Cart</button>';
                }
                ?>
            </form>
        </div>
    </div>
</div>
                <?php  } ?>
                <div class="col-lg-12">
    <ul class="pagination justify-content-center mt-5">
        <?php

        $sql = "SELECT * FROM product ";

        $records = mysqli_query($con, $sql);

        $totalRecords = mysqli_num_rows($records);

        $totalPage = ceil($totalRecords/$limit);

        echo "<li class='page-item'>
    <a href='index.php?page_no=1' class='page-link' id='1'>".'First page'."</a>
               </li>";

        for ($i=1;$i<=$totalPage;$i++) {
            echo "<li class='page-item'>
    <a href='index.php?page_no=".$i."' class='page-link' id='$i'>".$i."</a>
                </li>";
        }

        echo "<li>
    <a href='index.php?page_no=$totalPage' class='page-link' id='$totalPage'>".'Last Page'."</a>
            </li>";
        ?>
    </ul>
</div>
      <?php          }
            }
?>