<!-- start footer -->
<footer id="footer" class="bg-dark text-white py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-12">
                <h4 class="font-size-20">Coda Shope</h4>
                <p class="font-size-14 text-white-50">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repellendus, deserunt.</p>
            </div>
            <div class="col-lg-4 col-12">
                <h4 class="font-size-20">Newslatter</h4>
                <form class="form-row">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Email *">
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-primary mb-2">Subscribe</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-2 col-12">
                <h4 class="font-size-20">Information</h4>
                <div class="d-flex flex-column flex-wrap">
                    <a href="#" class="font-size-14 text-white-50 pb-1">About Us</a>
                    <a href="#" class="font-size-14 text-white-50 pb-1">Delivery Information</a>
                    <a href="#" class="font-size-14 text-white-50 pb-1">Privacy Policy</a>
                    <a href="#" class="font-size-14 text-white-50 pb-1">Terms & Conditions</a>
                </div>
            </div>
            <div class="col-lg-2 col-12">
                <h4 class="font-size-20">Account</h4>
                <div class="d-flex flex-column flex-wrap">
                    <a href="#" class="font-size-14 text-white-50 pb-1">My Account</a>
                    <a href="#" class="font-size-14 text-white-50 pb-1">Order History</a>
                    <a href="#" class="font-size-14 text-white-50 pb-1">Wish List</a>
                    <a href="#" class="font-size-14 text-white-50 pb-1">Newslatters</a>
                </div>
            </div>
        </div>
    </div>

</footer>
<!-- start footer -->

<div class="copyright text-center bg-dark text-white py-2">
    <p class="font-size-14">&copy; Copyrights 2021. Design By <a href="#" class="text-info">Fati Hasbi</a></p>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script src="styles/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="styles/OwlCarousel/owl.carousel.min.js"></script>

<link rel="stylesheet" href="style.css">
<script src="index.js"></script>
</body>
</html>