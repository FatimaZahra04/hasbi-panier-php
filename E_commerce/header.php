<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!-- bootstrap cdn -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/bootstrap/css/bootstrap.min.css">
    <!-- owl carousel cdn-->
    <link rel="stylesheet" href="styles/OwlCarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="styles/OwlCarousel/owl.theme.default.min.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- custom CSS file -->
    <link rel="stylesheet" href="style.css">
    <?php
    require_once ("database/db.php");

    require_once ("functions.php");
    require_once ("import/import_data.php");
    ?>

</head>
<body>

<!-- start #header -->
<header id="header">
    <div class="strip d-flex justify-content-between px-4 py-1 bg-light">
        <p class="font-size-12 text-black-50 m-0"><?php  echo isset($_SESSION['first_name'])? "Welcome back ".$_SESSION['first_name'] : "Try to login to proced to buy !" ?></p>
        <div class="font-size-14">
            <a href="<?php  echo isset($_SESSION['first_name'])? "logout.php" : "Login.php" ?>" class="px-3 border-right border-left text-dark"><?php  echo isset($_SESSION['first_name'])? "Logout" : "Login" ?></a>
        </div>
    </div>
    <!--primary navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php">Coda Shope</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav m-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">On Sale</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Category</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Products <i class="fa fa-chevron-down"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact Us</a>
                </li>
            </ul>
            <form action="#" class="font-size-14 ">
                <a href="cart.php" class="py-2 rounded-pill bg-primary">
                    <span class="font-size-16 px-2 text-white"><i class="fas fa-shopping-cart"></i></span>
                    <span class="px-3 py-2 rounded-pill text-dark bg-light panel"><?php echo count($product->getData('select * from cart')); ?></span>
                </a>
            </form>

        </div>
    </nav>

    <!--primary navigation -->

</header>
<!-- start #header -->
