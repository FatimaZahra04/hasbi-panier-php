<?php


class DBController
{
    //DB connection
    protected $host = 'localhost';
    protected $user = 'root';
    protected $password = '';
    protected $db_name = 'boutique';

    public $con=null;

    public function __construct()
    {
        $this ->con = mysqli_connect($this ->host,$this->user,$this->password,$this->db_name);

        if($this ->con ->connect_error) {
            echo "Connection failed".$this->con->connect_error;
        }
    }

    public function __destruct () {
        $this -> closeConnection();
    }

    //connection closing

    protected function closeConnection() {
        if ($this -> con = null) {
            $this -> con->close();
            $this -> con =null;
        }
    }

}
