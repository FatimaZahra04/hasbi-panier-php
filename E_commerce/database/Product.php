<?php


class Product
{
  public $db=null;

  public function __construct(DBController $db)
  {
      if (!isset($db->con)) return null;
      $this ->db=$db;
  }


  //fetch product data

    public function getData($sql='select * from product order by price ASC limit 0,8') {

     $result= $this->db->con->query($sql);

     $resultArray=array();
     while ($item=mysqli_fetch_array($result,MYSQLI_ASSOC)) {
         $resultArray[]=$item;
     }
     return $resultArray;
    }

    public function getProduct($sku_p=null,$table="product") {
      if (isset($sku_p)) {
          $result=$this->db->con->query('SELECT * from '.$table.' WHERE sku='.$sku_p.';');
          $resultArray=array();
          while ($item=mysqli_fetch_array($result,MYSQLI_ASSOC)) {
              $resultArray[]=$item;
          }
          return $resultArray;
      }
    }
}
