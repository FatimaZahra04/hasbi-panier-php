<?php

class cart {


    public $db=null;
    public function __construct(DBController $db) {
        if (!isset($db->con)) return null;
        $this->db=$db;
    }

    //insert into cart table

    public function insertIntoCart ($params = null,$table = "cart") {
        if ($this->db->con !=null) {
            if ($params!= null) {
                //"insert into cart(user_id) values(0)"
                //get table columns
                $columns=implode(',',array_keys($params));
                $values=implode(',',array_values($params));

                $query_string = sprintf("insert into %s(%s) values (%s)",$table,$columns,$values);
                $result=$this->db->con->query($query_string);
                return $result;
            }
        }
    }

    //GET user_id and sku product and insert into cart table

    public function addToCart($userId,$sku_p) {

      //  $curPageName=substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],"/")+1);


        if (isset($userId) && isset($sku_p)) {
            $params=array(
                "user_id" => $userId,
                "sku" => $sku_p
            );
            // insert data into cart
            $result=$this->insertIntoCart($params);
            if ($result) {
                //reload page
                header("Location:index.php");
            }
        }
    }
    //delete cart product using product sku

    public function deleteCart($product_sku,$table='cart') {

        if ($product_sku != null) {
            $result = $this->db->con->query("DELETE FROM {$table} where sku={$product_sku}");
        if ($result) {
            header("Location:cart.php");
        }
        return $result;
        }
    }


    public function getSum($arr) {
        if(isset($arr)) {
            $sum=0;
            foreach ($arr as $item) {
                $sum +=floatval($item[0]);
            }
            return sprintf("%.2f",$sum);
        }
    }

    //get product sku of shopping cart list
    public function getCartId($cartArray= null,$key = 'sku') {
        if ($cartArray !=null) {
            $cart_id=array_map(function ($value) use ($key){
                return $value[$key];
            },$cartArray);
            return $cart_id;
        }
    }

        //save for later

    public function saveForLater($product_id,$saveLatertable='wishlist',$DataTable='cart') {

        if ($product_id !=null) {
            $query="insert into {$saveLatertable} select * from {$DataTable} where sku={$product_id};";
            $query.="delete from {$DataTable} where sku={$product_id};";

            //execute multiple query

            $result= $this->db->con->multi_query($query);

            if ($result) {

                header("Location:cart.php");
            }
              return  $result;
        }
    }

}